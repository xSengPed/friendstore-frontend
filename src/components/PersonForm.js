import React from "react";
import { Input, Row, Col, Divider, Button, message } from "antd";
import "./css/Form.css";
import { useDispatch, useSelector } from "react-redux";
import { firestore } from "../index";
const PersonForm = () => {
  const dispatch = useDispatch();
  const form = useSelector((state) => state.form);
  const persons = useSelector((state) => state.person);
  const reset = () => {
    window.location.reload()
  }
  const addPerson = () => {
    dispatch({
      type: "ADD_PERSON",
      person: {
        ...form,
        id: persons.length > 0 ? persons[persons.length - 1].id + 1 : 0,
      },
    });
    firestore
      .collection("persons")
      .doc()
      .set({ ...form });
    message.success("A new person has been added");
  };
  return (
    <div className="outer-form">
      <div className="input-form">
        <p>
          <Divider>Create New Person</Divider>
        </p>
        <p>
          <Row gutter={10}>
            <Col span={12}>
              <Input
                placeholder="First Name"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_FIRST_NAME",
                    first_name: e.target.value,
                  })
                }
              />
            </Col>
            <Col span={12}>
              <Input
                placeholder="Last Name"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_LAST_NAME",
                    last_name: e.target.value,
                  })
                }
              />
            </Col>
          </Row>
        </p>
        <p>
          <Input
            placeholder="Nickname"
            onChange={(e) =>
              dispatch({
                type: "CHANGE_NICK_NAME",
                nick_name: e.target.value,
              })
            }
          />
        </p>
        <p>
          <Input
            placeholder="E-Mail"
            onChange={(e) =>
              dispatch({
                type: "CHANGE_EMAIL",
                email: e.target.value,
              })
            }
          />
        </p>
        <p>
          <Input
            placeholder="Tel"
            onChange={(e) =>
              dispatch({
                type: "CHANGE_TEL",
                tel: e.target.value,
              })
            }
          />
        </p>
        <p>
          <Input
            placeholder="University"
            onChange={(e) =>
              dispatch({
                type: "CHANGE_UNIVERSITY",
                university: e.target.value,
              })
            }
          />
        </p>
        <p>
          <Divider orientation="left">Address</Divider>
        </p>
        <p>
          <Row gutter={10}>
            <Col>
              <Input
                placeholder="House No."
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_HOME_ID",
                    home_id: e.target.value,
                  })
                }
              />
            </Col>
            <Col>
              <Input
                placeholder="Building"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_BUILDING",
                    building: e.target.value,
                  })
                }
              />
            </Col>
          </Row>
        </p>
        <p>
          <Row gutter={10}>
            <Col span={12}>
              <Input
                placeholder="Street"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_STREET",
                    street: e.target.value,
                  })
                }
              />
            </Col>
            <Col span={12}>
              <Input
                placeholder="District"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_DISTRICT",
                    district: e.target.value,
                  })
                }
              />
            </Col>
          </Row>
        </p>
        <p>
          <Row gutter={10}>
            <Col span={12}>
              <Input
                placeholder="Province"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_PROVINCE",
                    province: e.target.value,
                  })
                }
              />
            </Col>
            <Col span={12}>
              <Input
                placeholder="Zipcode"
                onChange={(e) =>
                  dispatch({
                    type: "CHANGE_ZIPCODE",
                    zip_code: e.target.value,
                  })
                }
              />
            </Col>
          </Row>
        </p>
        <p id="button-col">
          <Button type="primary" onClick={() => addPerson()}>
            Add
          </Button>
          {" "}
          <Button type="danger" onClick={()=>reset()}>
                Clear
          </Button>
        </p>
      </div>
    </div>
  );
};

export default PersonForm;
